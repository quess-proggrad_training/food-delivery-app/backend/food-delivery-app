package com.example.FoodDeliveryApp.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private UserDetailsService userDetailsService;

    AuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().disable();
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/addCart").hasAuthority("User")
                .antMatchers("/addFood").hasAuthority("Admin")
                .antMatchers("/delete/{id}").hasAuthority("Admin")
                .antMatchers("/getUserByID/{id}").hasAuthority("User")
                .antMatchers("/showUser").hasAuthority("Admin")
                .antMatchers("/showCart").permitAll()
                .antMatchers("/cartdelete/{itemid}").permitAll()
                .antMatchers("/addUser").hasAuthority(("Admin"))
                .antMatchers("/showFood").permitAll()
                .antMatchers("/delivery/{userid}").permitAll()
                .antMatchers("/showdelivery/{id}").permitAll()
                .anyRequest().authenticated().and().httpBasic();
    }
}