package com.example.FoodDeliveryApp.Service;

import com.example.FoodDeliveryApp.Model.Cart;
import com.example.FoodDeliveryApp.Repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CartService {
    @Autowired
    CartRepository cartRepositoryObj;
    public List<Cart> showCart() {
        return cartRepositoryObj.findAll();
    }

    public String insertCart(Cart cart) {
        cartRepositoryObj.save(cart);
        return "Cart Updated";
    }

    public void cartDelete(int itemid) {
        cartRepositoryObj.deleteById(itemid);
    }
}
