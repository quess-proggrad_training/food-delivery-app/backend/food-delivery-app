package com.example.FoodDeliveryApp.Service;

import com.example.FoodDeliveryApp.Model.Menu;
import com.example.FoodDeliveryApp.Repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuService {
    @Autowired
    MenuRepository menuRepositoryObj;

    public String addFood(Menu menuObj) {
        menuRepositoryObj.save(menuObj);
        return "Food Added";
    }

    public List<Menu> showMenu() {
        return menuRepositoryObj.findAll();
    }

    public void updateFood(Menu menuObj, int id) {
        Menu menu = menuRepositoryObj.findById(id).get();
        if (menu != null) {
           menuRepositoryObj.delete(menu);
            menuRepositoryObj.save(menuObj);
            System.out.println(menuObj.toString());
        }
    }

    public void remove(int id) {
        menuRepositoryObj.deleteById(id);
    }
}