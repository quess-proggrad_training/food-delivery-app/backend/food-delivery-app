package com.example.FoodDeliveryApp.Service;

import com.example.FoodDeliveryApp.Model.Users;
import com.example.FoodDeliveryApp.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepositoryObj;
    public String insertUser(Users userObj) {

        userRepositoryObj.save(userObj);
        return "User Added";
    }

    public List<Users> showUser() {
        return userRepositoryObj.findAll();
    }

    public Optional<Users> getbyID(int id) {
        return userRepositoryObj.findById(id);
    }
}
