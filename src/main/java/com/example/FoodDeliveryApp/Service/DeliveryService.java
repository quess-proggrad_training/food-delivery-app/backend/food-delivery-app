package com.example.FoodDeliveryApp.Service;

import com.example.FoodDeliveryApp.Model.DeliveryDetails;
import com.example.FoodDeliveryApp.Repository.DeliveryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DeliveryService {
    @Autowired
    DeliveryRepository deliveryRepositoryObj;
    public String addDelivery(DeliveryDetails deliveryDetails,int userid)
    {
        deliveryDetails.setNetAmt(deliveryRepositoryObj.netAmt(userid));
        deliveryRepositoryObj.save(deliveryDetails);
        return "Added";
    }

    public Optional<DeliveryDetails> show(int id) {
        return deliveryRepositoryObj.findById(id);
    }
}
