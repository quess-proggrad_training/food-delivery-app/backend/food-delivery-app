package com.example.FoodDeliveryApp.Repository;

import com.example.FoodDeliveryApp.Model.DeliveryDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DeliveryRepository extends JpaRepository<DeliveryDetails,Integer> {
    @Query(value = "select SUM(foodprice) AS netAmt FROM cart WHERE userid= :userid ", nativeQuery = true)
    public double netAmt(@Param("userid") Integer userid);


}
