package com.example.FoodDeliveryApp.Repository;

import com.example.FoodDeliveryApp.Model.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepository extends JpaRepository<Menu,Integer> {
}
