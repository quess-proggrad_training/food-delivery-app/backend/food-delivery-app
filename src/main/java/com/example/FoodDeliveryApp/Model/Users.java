package com.example.FoodDeliveryApp.Model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class Users {
    @Id
    private int userid;
    private String username;
    private String userAdd;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String userPass;
    private String userPhone;

    private String role;

    public Users() {
    }

    public Users(int userID, String userName, String userAdd, String userPass, String userPhone, String role) {
        this.userid = userID;
        this.username = userName;
        this.userAdd = userAdd;
        this.userPass = userPass;
        this.userPhone = userPhone;
        this.role = role;
    }

    public int getUserID() {
        return userid;
    }

    public void setUserID(int userID) {
        this.userid = userID;
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String userName) {
        this.username = userName;
    }

    public String getUserAdd() {
        return userAdd;
    }

    public void setUserAdd(String userAdd) {
        this.userAdd = userAdd;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }}



