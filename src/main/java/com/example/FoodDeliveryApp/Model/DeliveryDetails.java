package com.example.FoodDeliveryApp.Model;



import javax.persistence.*;


@Entity
public class DeliveryDetails {
    @Id
    private int deliveryID;
    @ManyToOne
    @JoinColumn(name="userid",referencedColumnName = "userid")
    private Users user;

    private double netAmt;

    public DeliveryDetails() {
    }

    public DeliveryDetails(int deliveryID) {
        this.deliveryID = deliveryID;
    }

    public int getDeliveryID() {
        return deliveryID;
    }

    public void setDeliveryID(int deliveryID) {
        this.deliveryID = deliveryID;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public double getNetAmt() {
        return netAmt;
    }

    public void setNetAmt(double netAmt) {
        this.netAmt = netAmt;
    }
}
