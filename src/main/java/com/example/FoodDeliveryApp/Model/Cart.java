package com.example.FoodDeliveryApp.Model;


import javax.persistence.*;

@Entity
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int itemID;
    @ManyToOne
    @JoinColumn(name ="userid",referencedColumnName = "userid")
    private Users user;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name="foodid", referencedColumnName="foodid"),
            @JoinColumn(name="foodprice", referencedColumnName="foodprice")})
    private Menu menu;

    public Cart() {
    }

    public Cart(Users user, Menu menu) {
        this.user = user;
        this.menu = menu;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}
