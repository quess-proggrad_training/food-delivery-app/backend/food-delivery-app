package com.example.FoodDeliveryApp.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="menu")
public class Menu implements Serializable {

  @Id
    private int foodID;
  private String foodName;
  private double foodPrice;




    public Menu() {
    }

    public Menu(int foodID,String foodName, double foodPrice) {
        this.foodName = foodName;
        this.foodPrice = foodPrice;
        this.foodID=foodID;
    }

    public int getFoodID() {
        return foodID;
    }

    public void setFoodID(int foodID) {
        this.foodID = foodID;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public double getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(double foodPrice) {
        this.foodPrice = foodPrice;
    }
}
