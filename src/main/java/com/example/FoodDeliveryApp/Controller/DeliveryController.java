package com.example.FoodDeliveryApp.Controller;

import com.example.FoodDeliveryApp.Repository.DeliveryRepository;
import com.example.FoodDeliveryApp.Model.DeliveryDetails;
import com.example.FoodDeliveryApp.Service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class DeliveryController {

    @Autowired
    DeliveryService deliveryServiceObj;
  @PostMapping ("/delivery/{userid}")
    public String insertFood(@RequestBody DeliveryDetails deliveryDetails, @PathVariable int userid)
    {
String str=deliveryServiceObj.addDelivery(deliveryDetails,userid);
return str;

    }
    @GetMapping("/showdelivery/{userid}")
    public Optional<DeliveryDetails> showDelivery(@PathVariable int userid)
    {
        return deliveryServiceObj.show(userid);
    }
}
