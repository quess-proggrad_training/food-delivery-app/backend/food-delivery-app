package com.example.FoodDeliveryApp.Controller;

import com.example.FoodDeliveryApp.Model.Cart;
import com.example.FoodDeliveryApp.Model.Menu;
import com.example.FoodDeliveryApp.Repository.CartRepository;
import com.example.FoodDeliveryApp.Service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CartController {
@Autowired
    CartService cartServiceObj;
@PostMapping("/addCart")
public String addCart(@RequestBody Cart cart)
{
    String str= cartServiceObj.insertCart(cart);
    return str;
}
    @GetMapping("/showCart")
    public List<Cart> getCart()
    {

        return cartServiceObj.showCart();
    }
    @DeleteMapping("/cartdelete/{itemid}")
    public void deletecart(@PathVariable int itemid)
    {
        cartServiceObj.cartDelete(itemid);
    }

}
