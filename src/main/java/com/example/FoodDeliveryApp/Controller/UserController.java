package com.example.FoodDeliveryApp.Controller;

import com.example.FoodDeliveryApp.Model.Users;
import com.example.FoodDeliveryApp.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    UserService userServiceObj;
    @PostMapping("/addUser")
    public String addUser(@RequestBody Users usersObj)
    {
        String str= userServiceObj.insertUser(usersObj);
        return str;
    }
@GetMapping("/showUser")
    public List<Users> getUser()
    {
             return userServiceObj.showUser();
    }
    @GetMapping("/getUserByID/{id}")
    public Optional<Users> getUserId(@PathVariable int id)
    {
        return userServiceObj.getbyID(id);
    }

}
