package com.example.FoodDeliveryApp.Controller;

import com.example.FoodDeliveryApp.Model.Menu;
import com.example.FoodDeliveryApp.Service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MenuController {

    @Autowired
    MenuService menuServiceObj;
    @PostMapping("/addFood")
    public String insertFood(@RequestBody Menu menuObj)
    {
        String str= menuServiceObj.addFood(menuObj);
        return str;
    }
    @GetMapping("/showFood")
    public List<Menu> getMenu()
    {
        return menuServiceObj.showMenu();
    }

    @PutMapping("/update/{id}")
    public void update(@RequestBody Menu menuObj, @PathVariable int id)
    {
        menuServiceObj.updateFood(menuObj,id);
    }
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable int id)
    {
        menuServiceObj.remove(id);
    }
}
